﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_prog_odev.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Anasayfa()
        {
            return View();
        }
        public ActionResult Futbol()
        {
            return View();
        }
        public ActionResult Canli_Sonuclar()
        {
            return View();
        }
        public ActionResult Oy_Ver()
        {
            return View();
        }
        public ActionResult Fikstür()
        {
            return View();
        }
        public ActionResult Kunye()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult Giris_Yap()
        {
            return View();
        }
        public ActionResult Kayit_Ol()
        {
            return View();
        }
    }
}